{ stdenv
, lib
, fetchzip
}:

stdenv.mkDerivation {
  pname = "asxxxx";
  version = "5.50";

  src = fetchzip {
    url = "https://shop-pdp.net/_ftp/asxxxx/asxs5p50.zip";
    sha256 = "sha256-BNU3qhfxINmLwgzA7rtbWoc7evJQLMYmwEIvvcaJsI4=";
    stripRoot = false;
  };

  buildInputs = [];

  enableParallelBuilding = true;

  buildPhase = ''
    pushd asxv5pxx/asxmak/linux/build
    buildPhase
    popd
  '';

  installPhase = ''
    mkdir -p $out/bin
    cp asxv5pxx/asxmak/linux/exe/* $out/bin
  '';

  meta = with lib; {
    homepage = "https://shop-pdp.net/ashtml";
    description = "ASxxxx Cross Assemblers";
    platforms = platforms.linux;
  };
}
