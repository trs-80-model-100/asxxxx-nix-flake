{
  inputs = {
    m100-utils-flake.url = "gitlab:trs-80-model-100/utils";
  };
  outputs = { self, nixpkgs, flake-utils, m100-utils-flake }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};

        asxxxx = pkgs.callPackage ./asxxxx.nix {};

        m100-utils = m100-utils-flake.packages.${system}.default;
      in
      {
        packages.default = asxxxx;

        devShells.default = with pkgs; mkShell {
          packages = [
            asxxxx
            gdb
            m100-utils
          ];

          buildInputs = [
            gcc
            gnumake
          ];

          M100_UTILS_MK = "${m100-utils}/make";
        };
      }
    );
}
