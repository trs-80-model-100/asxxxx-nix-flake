.8085
.area CODE (ABS)
.ifdef CO_HEAD
  .org 0xEA60 - 6 ; .CO header is 6 bytes long
.else
  .org 0xEA60
.endif
.end _start

.define $print "0x11A2"
.define $chget "0x12DB"

.ifdef CO_HEAD
  .word 0xEA60         ; origin
  .word $bottom - $top ; size
  .word _start         ; transfer
.endif

$top:

_start:
        ; clear screen
        mvi a, 0x0C
        rst 4

        mvi a, 0
        mvi b, 5

1$:
        dcr b
        mvi h, >$msg
        mvi l, <$msg
        call $print
        cmp b
        jnz 1$

        call $chget

        ret

$msg:
        .asciz "Hello, world!\n\r"

$bottom:

.ifdef CO_HEAD
  .byte 0x00 ; EOF marker
.endif
